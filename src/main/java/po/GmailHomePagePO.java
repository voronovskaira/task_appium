package po;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class GmailHomePagePO extends BasePage {

    public GmailHomePagePO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id = "com.google.android.gm:id/compose_button")
    private MobileElement writeAMessage;

    @AndroidFindBy(id = "com.google.android.gm:id/to")
    private MobileElement receiver;

    @AndroidFindBy(id = "com.google.android.gm:id/subject")
    private MobileElement subject;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.webkit.WebView/android.webkit.WebView/android.widget.EditText")
    private MobileElement textOfMessage;

    @AndroidFindBy(id = "com.google.android.gm:id/send")
    private MobileElement sendAMessage;

    @AndroidFindBy(id = "com.google.android.gm:id/description_text")
    private MobileElement notificationMessageSending;


    public GmailHomePagePO clickOnWriteAMessageButton() {
        waitElementToBeClickable(writeAMessage);
        writeAMessage.click();
        return this;
    }

    public GmailHomePagePO writeAMessageTo(String email) {
        waitElementToBeVisible(receiver);
        receiver.sendKeys(email);
        return this;
    }

    public GmailHomePagePO writeASubjectOfMessage(String subject) {
        this.subject.sendKeys(subject);
        return this;
    }

    public GmailHomePagePO writeAMessage(String message) {
        textOfMessage.click();
        textOfMessage.sendKeys(message);
        return this;
    }

    public GmailHomePagePO sendAMessage() {
        sendAMessage.click();
        return this;
    }

    public boolean verifyMessageSent() {
        return notificationMessageSending.isDisplayed();
    }
}
