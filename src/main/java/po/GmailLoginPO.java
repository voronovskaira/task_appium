package po;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class GmailLoginPO extends BasePage {

    @AndroidFindBy(id = "com.google.android.gm:id/welcome_tour_got_it")
    private MobileElement gotItButton;

    @AndroidFindBy(id = "com.google.android.gm:id/action_done")
    private MobileElement goToGmailButton;

    @AndroidFindBy(id = "com.google.android.gm:id/setup_addresses_add_another")
    private MobileElement addEmailButton;

    @AndroidFindBy(id = "com.google.android.gm:id/account_setup_item")
    private MobileElement googleAccountButton;

    @AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[3]/android.view.View/android.view.View[1]/" +
            "android.view.View[1]/android.widget.EditText")
    private MobileElement emailInput;

    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private MobileElement nextForSubmitEmailButton;

    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[3]/android.view.View/" +
            "android.view.View/android.view.View/android.view.View/android.view.View[1]/android.widget.EditText")
    private MobileElement passwordInput;

    @AndroidFindBy(id = "com.google.android.gms:id/minute_maid")
    private MobileElement head;

    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private MobileElement nextForSubmitPasswordButton;

    @AndroidFindBy(xpath = "//android.widget.Button[contains(@text,'Прийняти')]")
    private MobileElement acceptTermsAndConditionsButton;

    @AndroidFindBy(id = "com.google.android.gms:id/sud_navbar_more")
    private MobileElement swipeDown;

    @AndroidFindBy(id = "com.google.android.gms:id/sud_navbar_next")
    private MobileElement acceptGoogleServiceButton;

    @AndroidFindBy(id = "com.google.android.gm:id/owner")
    private MobileElement owner;


    public GmailLoginPO(AppiumDriver driver) {
        super(driver);
    }

    public GmailLoginPO clickOnGotItButton() {
        gotItButton.click();
        return this;
    }

    public GmailLoginPO clickOnAddEmailButton() {
        addEmailButton.click();
        return this;
    }

    public GmailLoginPO chooseGoogleAccount() {
        waitElementToBeVisible(googleAccountButton);
        googleAccountButton.click();
        return this;
    }

    public GmailLoginPO waitAndInputEmail(String email) {
        waitElementToBeVisible(emailInput);
        emailInput.sendKeys(email);
        return this;
    }

    public GmailLoginPO submitEmail() {
        nextForSubmitEmailButton.click();
        return this;
    }

    public GmailLoginPO inputPassword(String password) {
        waitElementToBeVisible(passwordInput);
        head.click();
        passwordInput.sendKeys(password);
        return this;
    }

    public GmailLoginPO submitPassword() {
        nextForSubmitPasswordButton.click();
        return this;
    }

    public GmailLoginPO acceptTermsAndConditions() {
        waitElementToBeVisible(acceptTermsAndConditionsButton);
        acceptTermsAndConditionsButton.click();
        waitElementToBeVisible(swipeDown);
        swipeDown.click();
        acceptGoogleServiceButton.click();
        return this;
    }

    public GmailLoginPO goToGmail() {
        waitElementToBeVisible(owner);
        goToGmailButton.click();
        return this;
    }
}
