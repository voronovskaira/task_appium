package driver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

public class DriverManager {

    private static AppiumDriver driver;
    private static DesiredCapabilities caps = new DesiredCapabilities();


    private static DesiredCapabilities setCapabilities() {
        caps.setCapability("deviceName", "SM-A300H");
        caps.setCapability("udid", "b16dc47a");
        caps.setCapability("platformName", "Android");
        caps.setCapability("appActivity", "GmailActivity");
        caps.setCapability("appPackage", "com.google.android.gm");
        return caps;
    }

    public static AppiumDriver getDriver() {
        return Objects.isNull(driver) ? createDriver() : driver;
    }

    private static AppiumDriver createDriver() {
        try {
            driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), setCapabilities());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return driver;
    }


    public static void quit() {
        driver.quit();
    }
}
