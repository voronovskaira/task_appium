package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "message")
public class Message {
    private String subject;
    private String receiver;
    private String message;

    public String getSubject() {
        return subject;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getMessage() {
        return message;
    }

}
