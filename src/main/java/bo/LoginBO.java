package bo;

import driver.DriverManager;
import po.GmailLoginPO;

public class LoginBO {
    GmailLoginPO gmailLoginPO;

    public LoginBO() {
        this.gmailLoginPO = new GmailLoginPO(DriverManager.getDriver());
    }

    public void loginUser(String email, String password) {
        gmailLoginPO.clickOnGotItButton()
                .clickOnAddEmailButton()
                .chooseGoogleAccount()
                .waitAndInputEmail(email)
                .submitEmail()
                .inputPassword(password)
                .submitPassword()
                .acceptTermsAndConditions()
                .goToGmail();
    }
}
