package bo;

import driver.DriverManager;
import model.Message;
import po.GmailHomePagePO;

public class HomePageBO {
    private GmailHomePagePO gmailHomePagePO;

    public HomePageBO() {
        this.gmailHomePagePO = new GmailHomePagePO(DriverManager.getDriver());
    }

    public void writeAndSendMessage(Message message) {
        gmailHomePagePO.clickOnWriteAMessageButton()
                .writeAMessageTo(message.getReceiver())
                .writeASubjectOfMessage(message.getSubject())
                .writeAMessage(message.getMessage())
                .sendAMessage();
    }

    public boolean verifyMessageSent() {
        return gmailHomePagePO.verifyMessageSent();
    }
}
